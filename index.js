// console.log("Hello World");

// [SECTION] Functions
	// Functions in javascript are line/blocks of codes that tell our device/application to perform a specific tasks when called/invoked.
	// it prevents repeating lines/blocks of codes that perform the same task/function .

// Function Declarations
	// function statement is defined by a function.
		
	/*
		Syntax:
			function functionName () {	
				code block (statement)
			}

	- function keyword - used to define a javascript function.
	- functionName - name of the function, which will be used to call/invoke the function.
	- function block ({}) - indeciates the function body.
	*/
	
	function printName () {
		console.log ("My Name is John");
	}

// Function Invocation
	// This run/execute the code block inside the function.

	printName();

	declaredFunction(); //we cannot invoke a function that we have not yet declared/defined.

// [SECTION] Function Declarations vs Funstion Expressions

	// Function Declaration
		// function can be created by using function keyword and adding a function name.
	// "saved for later use"
	// Declared functions can be "hoisted", as long a function has been defined.
		// Hoisting is JS behavior for "certain variable" (var) and functions to run or use them before their declaration.

	function declaredFunction() {
		console.log("Hello World from declaredFunction");
	}

	// Function Expression
		// a function can also be stored in a variable

	/*
		Syntax:
			let/const variableName = function() {
				// code block (statement)
			}

			-function(){} - Anonymous function, a function without a name.
	*/

	// variableFunction(); // error - function expression, being stored in a let/const variable, cannot be hoisted.

	let variableFunction = function(){
		console.log("Hello Again!");
	}

	variableFunction();

	// We can also create function expression with a named function.

	let funcExpression = function funcName(){
		console.log("Hello from the other side.");
	}

	// funcName; //funcName() is not defined
	funcExpression(); // to invoke the function, we invoke it by its variable name and not by its function name.

	declaredFunction = function(){
		console.log("updated declaredFunction");
	}

	declaredFunction();

	funcExpression = function(){
		console.log("updated funcExpression");
	}

	funcExpression();

	// we cannot re-assign a function expression initialized with cont.

	const constantFunc = function(){
		console.log("Initialized with const");
	}

	constantFunc();

	// This will result to reassignment error
	// constantFunc = function(){
	// 	console.log("Cannot be reassigned");
	// }
	// constantFunc();

// [SECTION] Function Scoping

	/*
		Scopr is the accessibility (visibility) of vairables. 

		JavaScript Variables has 3 types of scope:
		1. global scope
		2. local scope
		3. 
	*/

	// Global Scope
		// variable can be accessed anywhere from the program.

	let globarlVar = "Mr. WorldWide";

	// Local Scope
		// variables declared inside a curly bracket({}) can only be accessed locally.
	
		// console.log(localVar); //not used now, "Var" can also perform hoisting, can be used it global and local

		{
			// var localVar = "Armando Perez";
			let localVar = "Armando Perez";
		}

		console.log(globarlVar);
		// console.log(localVar); // result in error. cannot be accessed outside its code block

	// Function Scope
		// Each Function creates a new scope.
		// Variables defined inside a function are not accessible from outside the function.

	function showNames(){
		// Functon Scope Variable
		var functionVar = "Joe";
		const functionConst = "John";
		let functionLet = "Joey";

		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);
	}

	showNames();

	// console.log(functionVar);
	// console.log(functionConst);
	// console.log(functionLet);

	function myNewFunction(){
		let name = "Jane";

		function nestedFunc(){
			let nestedName = "John";
			console.log(name);

			console.log(nestedName); 
		}

		// console.log(nestedName); // error, it is outside its function (parent-to-child relationship only,)
		nestedFunc();
	}
	myNewFunction();

	// nestedFunc(); //result to an error.

	// Global Scope Variable

	let globalName = "Alex";
	function myNewFunction2(){
		let nameInside = "Renz";
		console.log(globalName);
		console.log(nameInside);
	}

	myNewFunction2();

	//console.log(nameInside); // error, only accessible on the function scope.

// [SECTION] Using alert() and prompt()
	// alert() allows us to show small window at the top of our browser page to show information to our users.

		alert("Youkosoo!! Atarashii Sekai!!"); // This will run immediately when the page reloads.

		// You can use alert() to show a message to the user from a later function invocation.

		function showSampleAlert(){
			alert("Konnichiwa, Kimi!");
		}

		showSampleAlert();

		console.log("I will only log in the console when the alert is dismissed");

		/*
			Notes on the use of alert(): 
			- Show only an alert() for short dialogues/messages to the user.
			- DO not overuse alert() because the program has to wait for it to be dismissed before it continues.
		*/

	// prompt() allows us to show a small window at the top of the browser to gather user input.
		// the input from the prompt() will be returned as a STRING once the user dismissed the window.

		/*
			Sytnax: 
				let/const variableName = prompt("<dialogInString>");
		*/

		let name = prompt("Enter your name: ");
		let age = prompt("Enter your age: ");

		console.log(typeof age); //returned value of prompt

		console.log("Hello, I am "+name+", and I am "+age+" years old.");

		let sampleNullPrompt = prompt("Do Not Enter Anything");

		console.log(sampleNullPrompt);
		// prompt() returns an "empty string" ("") when there is no user input and we have clicked okay. or "null" if the user cancels the input.

		function printWelcomeMessage(){
			let name = prompt("Enter your name: ");

			console.log("Hello, "+name+"! Welcome to my page!");
		}

		printWelcomeMessage();

// [SECTION] Function Naming Conventions

	// 1. Function names should be definitive of the task it wil perform. it usually contains a verb.

		function getCourses(){
			let courses = ["English 101","Science 101","Math 101"]
			console.log(courses);
		}

		getCourses
	// 2. Avoid Generic names to avoid confusion within your code.

		function get(){
			let name = "Jamie";
			console.log(name);
		}

		get();

	// 3. Avoid pointless and inappropriate function names, e.g. foo, bar, etc... (metasyntactic variable - placeholder variables).

		function foo(){
			console.log(25%5)
		}

		foo();